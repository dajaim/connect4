-module(c4Client).

-export([main/0]).

-include("typedefs.hrl").
xd()->
	timer:sleep(1000),
	io:write(in_xd),
	xd().

playGame(C4ServerProcessId)->
	C4ServerProcessId ! {getGameContext,self()},
	
	receive 
		{gameOver,GameContext}-> 
			PlayerTurn = GameContext#gameContext.playerTurn,
			c4Utils:printField(GameContext#gameContext.field),
			io:fwrite("player "), io:write(PlayerTurn), io:fwrite(" won!"),
			
			ok;

		GameContext -> 
			c4Utils:printGameContext(GameContext),
		
			{ok,ColumnCandidate} = io:read("Enter column index: "),
		
			PlayerTurn = GameContext#gameContext.playerTurn,

			C4ServerProcessId ! {turnAttempt,#turnContext{sender=self(),playerTurn=PlayerTurn,columnIndex=ColumnCandidate}},
			
			playGame(C4ServerProcessId)
	end.


receiveServerProcessId()->
	receive
		{c4ServerProcessId,C4ServerProcessId}->C4ServerProcessId
	end.

main()->
	C4ServerProcessId = receiveServerProcessId(),
	playGame(C4ServerProcessId).