-module(c4Server).

-include("typedefs.hrl").

-export([main/2,makeC4GameContext/0]).

makeListOfLists(0)->
	[];
makeListOfLists(AmountOfLists)->
	makeListOfLists(AmountOfLists-1)++[[]].

makeCustomC4GameContext(Width,Height)->
	Field = makeListOfLists(Width),

	PlayerTurn = rand:uniform(2),

	#gameContext{playerTurn=PlayerTurn,field=#field{bottomXAxis=Field,height=Height,width=Width}}.

makeC4GameContext()->
	makeCustomC4GameContext(7,6).


directGame(GameContext,ServerForeignProcessIds)->
	receive 
		{getGameContext,Sender}-> 
			Sender ! GameContext,
			directGame(GameContext,ServerForeignProcessIds);
		{turnAttempt,TurnContext}-> 
		
			ProcessTurnResult = processTurnAttempt(GameContext,TurnContext),
			
			case ProcessTurnResult of
				{playerWon, NewGameContext }->TurnContext#turnContext.sender ! {gameOver,NewGameContext},ok;
				{newGameContext, NewGameContext }->directGame(NewGameContext,ServerForeignProcessIds);
				{invalidTurn,OldGameContext}->directGame(OldGameContext,ServerForeignProcessIds)
			end

	end.

isTurnInBoundaries(GameContext,TurnContext)->
	% consider column ( x ) boundaries and height ( y ) boundaries
	ColumnIndex = TurnContext#turnContext.columnIndex,
	Field = GameContext#gameContext.field,
	FieldWidth = Field#field.width,

	ValidColumn = ((ColumnIndex >0) and (ColumnIndex =< FieldWidth)),
	NotFullColumn = not c4Utils:isColumnFull(Field,ColumnIndex),

	ValidColumn and NotFullColumn.

validateTurnAttempt(GameContext,TurnContext)->
	IsTurnInBoundaries = isTurnInBoundaries(GameContext,TurnContext),

	%  other stuff

	IsTurnInBoundaries.

getPlayerTurnCorrespondingSymbol(PlayerTurn)->
	case PlayerTurn of
		1 -> x;
		2 -> o
	end.


getNewPlayerTurn(PlayerTurn)->
	case PlayerTurn of
		1->2;
		2->1
	end.

processTurnAttempt(GameContext,TurnContext)->
	ValidationResult = validateTurnAttempt(GameContext,TurnContext),

	if ValidationResult ->
		PlayerTurn = TurnContext#turnContext.playerTurn,
		ColumnIndex = TurnContext#turnContext.columnIndex,
		Field = GameContext#gameContext.field,
		
		UpdatedField = c4Utils:insertSymbolInFieldInColumn(getPlayerTurnCorrespondingSymbol(PlayerTurn),Field,ColumnIndex),

		HasSymbolAQuadInField = c4Utils:hasSymbolAQuadInField(UpdatedField,getPlayerTurnCorrespondingSymbol(PlayerTurn)),

		case HasSymbolAQuadInField of
			true->{playerWon,#gameContext{field=UpdatedField,playerTurn=PlayerTurn}};
			false->{newGameContext,#gameContext{field=UpdatedField,playerTurn=getNewPlayerTurn(PlayerTurn)}}
		end;
	true->
		{invalidTurn,GameContext}
	end.


tellC4ClientC4ServerProcessId(ClientProcessId)->
	ClientProcessId ! {c4ServerProcessId,self()}.


main(MainProcessId,ClientProcessId)->
	tellC4ClientC4ServerProcessId(ClientProcessId),

	directGame(
		makeC4GameContext(),
		#serverForeignProcessIds{
			mainProcessId=MainProcessId,
			clientProcessId=ClientProcessId
		}
	).