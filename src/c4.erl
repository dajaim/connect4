-module(c4).

-export([main/0]).

main()->
    ClientProcessId = spawn(c4Client,main,[]),
    spawn(c4Server,main,[self(),ClientProcessId]),
    
    receive 
        serverDown->ok  
    end.