-module(c4Utils).

-include("typedefs.hrl").

-define(Default,i).

-export([
printGameContext/1,
test/0,
makeCoordinatesListCorrespondingToField/1,
isColumnFull/2,
insertSymbolInFieldInColumn/3,
hasSymbolAQuadInField/2,
printField/1
]).


isColumnFull(Field,ColumnIndex)->
    Column = getColumnFromField(Field,ColumnIndex),
    length(Column) == Field#field.height.

getRowFromField(Field,RowIndex)->
    extractElementsOfRowFromField(Field,RowIndex,1).

extractElementsOfRowFromField(Field,RowIndex,ColumnIndex)->
    if ColumnIndex > Field#field.width ->
        [];
        true-> [getFieldEntryOrDefault(Field,ColumnIndex,RowIndex)|extractElementsOfRowFromField(Field,RowIndex,ColumnIndex+1)]
    end.

getColumnFromField(Field,ColumnIndex)->
    BottomXAxis = Field#field.bottomXAxis,
    lists:nth(ColumnIndex,BottomXAxis).

getFieldEntryOrDefault(Field,X,Y)->
    BottomXAxis = Field#field.bottomXAxis,

    if 
        (X =< Field#field.width) and (Y =< Field#field.height)->% sind die verlangtend werte (X und Y) innerhalb des feldes?
            
            Column = lists:nth(X,BottomXAxis),

            if 
                Y > length(Column)->%ist die yachse für den erfragten y wert NICHT ausreichend groß?
                    ?Default;
                true->
                    lists:nth(Y,Column)
            end;
        true->
            ?Default
    end.

makeSubList(List,BeginIndex,EndIndex)->
    lists:sublist(List,BeginIndex,EndIndex-BeginIndex).

insertSymbolInFieldInColumn(Symbol,Field,ColumnIndex)->

    Column = getColumnFromField(Field,ColumnIndex),
    
    if length(Column) == Field#field.height->
        Field;
    true->
        BottomXAxis = Field#field.bottomXAxis,
        Width = Field#field.width,

        ColumnWithSymbol = Column++[Symbol],
        ColumnsBeforeColumnWithSymbol = makeSubList(BottomXAxis,1,ColumnIndex),
        ColumnsAfterColumnWithSymbol = makeSubList(BottomXAxis,ColumnIndex+1,Width+1),

        #field{ 
            bottomXAxis= ColumnsBeforeColumnWithSymbol++[ColumnWithSymbol]++ColumnsAfterColumnWithSymbol,
            height = Field#field.height,
            width = Field#field.width
        }
    end.

printRow(Row)->
    if Row == []->
        io:nl();
    true->
        [Head|Tail] = Row,
        io:fwrite(Head), 
        io:fwrite(" "),
        printRow(Tail)
    end.


printFieldRowsBeginAtHeight(Field,CurrentRowIndex)->
    if CurrentRowIndex == 0->ok;
    true->
        CurrentRow = getRowFromField(Field,CurrentRowIndex),
        printRow(CurrentRow),
        printFieldRowsBeginAtHeight(Field,CurrentRowIndex-1)
    end.


    
printField(Field)->
    Height = Field#field.height,
    printFieldRowsBeginAtHeight(Field,Height).




printPlayerTurn(PlayerTurn)->
    io:fwrite("player turn "),
    io:write(PlayerTurn),
    io:nl().


printGameContext(GameContext)->
    printPlayerTurn(GameContext#gameContext.playerTurn),
    io:nl(),
    printField(GameContext#gameContext.field),
    io:nl().


areCoordinatesInBoundsCorrespondingToField(Field,Coordinates)->
    XCoordinates = Coordinates#coordinates.x,
    YCoordinates = Coordinates#coordinates.y,

    FieldWidth = Field#field.width,
    FieldHeight = Field#field.height,

    (XCoordinates>0) and (XCoordinates =< FieldWidth) and
    (YCoordinates>0) and (YCoordinates =< FieldHeight).

doCoordinatesContainOutOfBoundsCoordinatesCorrespondingToField(Field,CoordinatesList)->
    if CoordinatesList == []->
        true;
    true->
        [Coordinates|RestCoordinates]=CoordinatesList,

        areCoordinatesInBoundsCorrespondingToField(Field,Coordinates)
        and
        doCoordinatesContainOutOfBoundsCoordinatesCorrespondingToField(Field,RestCoordinates)

    end.

tryMakeingAValidQuadCorrespondingToField(Field,CurrentCoordinates,DeltaCoordinates)->
    tryMakeingAValidQuadCorrespondingToField(Field,CurrentCoordinates,DeltaCoordinates,4).

tryMakeingAValidQuadCorrespondingToField(Field,CurrentCoordinates,DeltaCoordinates,AmountOfRemainingCoordinates)->
    if AmountOfRemainingCoordinates == 0->
        [];
    true->
        CurrentXCoordinate = CurrentCoordinates#coordinates.x,
        CurrentYCoordinate = CurrentCoordinates#coordinates.y,
        
        NextXCoordinate = CurrentXCoordinate+DeltaCoordinates#coordinates.x,
        NextYCoordinate = CurrentYCoordinate+DeltaCoordinates#coordinates.y,

        AreCoordinatesInBoundsCorrespondingToField = areCoordinatesInBoundsCorrespondingToField(Field,CurrentCoordinates),

        if AreCoordinatesInBoundsCorrespondingToField ->
            [CurrentCoordinates]++tryMakeingAValidQuadCorrespondingToField(Field,#coordinates{x=NextXCoordinate,y=NextYCoordinate},DeltaCoordinates,AmountOfRemainingCoordinates-1);
        true-> 
            []
        end
    end.





getCoordinatesAsQuadsFromField(Field,CurrentCoordinates,DeltaCoordinates)->
    CurrentXCoordinate = CurrentCoordinates#coordinates.x,
    CurrentYCoordinate = CurrentCoordinates#coordinates.y,
    
    NextIterationXCoordinate = CurrentXCoordinate+1,

    WidthEnd = Field#field.width+1,
    HeigthEnd = Field#field.height+1,
    
    AreCurrentCoordinatesInBoundsCorrespondingToField = areCoordinatesInBoundsCorrespondingToField(Field,CurrentCoordinates),

    if AreCurrentCoordinatesInBoundsCorrespondingToField ->
        ok;
        true->ok
    end.

makeCoordinatesListWithXAndYCoordinates(X,Ys)->
    if Ys == []->
        [];
    true->
        [Y|YRest] = Ys,
        [#coordinates{x=X,y=Y}|makeCoordinatesListWithXAndYCoordinates(X,YRest)]
    end.

makeCoordinatesListCorrespondingToField(Field)->
    makeCoordinatesListCorrespondingToField(Field,Field#field.width).

makeCoordinatesListCorrespondingToField(Field,XCoordinate)->
    if (XCoordinate == 0) ->
        [];
    true->
        makeCoordinatesListWithXAndYCoordinates(
            XCoordinate,
            lists:seq(
            	1,
                Field#field.height
            )
        )++
        makeCoordinatesListCorrespondingToField(Field,XCoordinate-1)
    end.

makeTopLeftToBotRightDeltaCoordinates()->
    #coordinates{x=-1,y=-1}.

makeTopToBotDeltaCoordinates()->
    #coordinates{x=0,y=1}.

makeLeftToRightDeltaCoordinates()->
    #coordinates{x=1,y=0}.

makeBotLeftToTopRightDeltaCoordinates()->
    #coordinates{x=1,y=1}.

makeValidQuads(Field,DirectionDeltaCoordinates)->
    makeValidQuads(Field,DirectionDeltaCoordinates,makeCoordinatesListCorrespondingToField(Field)).

makeValidQuads(Field,DirectionDeltaCoordinates,ValidCoordinatesBeginningList)->
    if( ValidCoordinatesBeginningList ==  [])->
        [];
    true->
        [ValidCoordinates|ValidCoordinatesRestList]= ValidCoordinatesBeginningList,

        ValidQuadCandidate = tryMakeingAValidQuadCorrespondingToField(Field,ValidCoordinates,DirectionDeltaCoordinates),

        NextValidQuads = makeValidQuads(Field,DirectionDeltaCoordinates,ValidCoordinatesRestList),

        if (length(ValidQuadCandidate)==4)->
            [ValidQuadCandidate|NextValidQuads];
        true->
            NextValidQuads
        end
    end.

getEntriesFromField(Field,CoordinatesList)->
    if (CoordinatesList == [])->
        [];
    true->
        [Coordinates|RestCoordinatesList] = CoordinatesList,

        XCoordinate = Coordinates#coordinates.x,
        YCoordinate = Coordinates#coordinates.y,

        FieldEntry = getFieldEntryOrDefault(Field,XCoordinate,YCoordinate),

        [FieldEntry|getEntriesFromField(Field,RestCoordinatesList)]
    end.

getQuadEntriesFromField(Field,Quads)->
    if Quads==[]->
        [];
    true->
        [Quad|RestQuads]=Quads,

        CoordinatesList = Quad,

        QuadEntries = getEntriesFromField(Field,CoordinatesList),

        [QuadEntries|getQuadEntriesFromField(Field,RestQuads)]
    end.

hasSymbolAQuadInField(Field,Symbol)->
    Quads = 
        makeValidQuads(Field,makeBotLeftToTopRightDeltaCoordinates())++
        makeValidQuads(Field,makeLeftToRightDeltaCoordinates())++
        makeValidQuads(Field,makeTopLeftToBotRightDeltaCoordinates())++
        makeValidQuads(Field,makeTopToBotDeltaCoordinates()),
    
    isAnyQuadInFieldConsistingOfSymbolOnly(Field,Quads,Symbol).

isAnyQuadInFieldConsistingOfSymbolOnly(Field,Quads,Symbol)->
    QuadFieldEntries = getQuadEntriesFromField(Field,Quads),

    lists:any(
        fun (QuadFieldEntry)-> lists:all(
            fun(SymbolCandidate)->(
                SymbolCandidate ==Symbol
            )
            end,
            QuadFieldEntry
        )
        end,
        QuadFieldEntries
    ).



test()->
    GameContext = c4Server:makeC4GameContext(),

    {_,FieldWithCoin} = insertSymbolInFieldInColumn(o,GameContext#gameContext.field,1),
    {_,F} = insertSymbolInFieldInColumn(o,FieldWithCoin,1),
    {_,D} = insertSymbolInFieldInColumn(o,F,1),
    {_,E} = insertSymbolInFieldInColumn(o,D,4),
    {_,Q} = insertSymbolInFieldInColumn(o,E,2),
    {_,W} = insertSymbolInFieldInColumn(o,Q,4),
    io:write(hasSymbolAQuadInField(W,o)).
