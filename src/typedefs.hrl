-export([]).

-record(coordinates,{x,y}).


-record(field,{bottomXAxis,height,width}).

-record(gameContext,{playerTurn,field}).

-record(turnContext,{sender,playerTurn,columnIndex}).

-record(turnValidation,{}).

-record(gameState,{sender,gameContext}).



-record(serverForeignProcessIds,{mainProcessId,clientProcessId}).

-record(clientForeignProcessIds,{serverProcessId}).